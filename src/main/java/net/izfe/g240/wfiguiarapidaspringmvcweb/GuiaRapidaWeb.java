package net.izfe.g240.wfiguiarapidaspringmvcweb;

import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.XmlViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import net.izfe.g240.wfiframeworkizfelib.springmvc.excepciones.IzfeSimpleMappingExceptionResolver;


@EnableWebMvc
@ComponentScan
@Configuration
//@Import({ GuiaRapidaCore.class })
public class GuiaRapidaWeb extends WebMvcConfigurerAdapter {

	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/estatico/**").addResourceLocations("/estatico/");
	}

//	@Override
//	public void addViewControllers(final ViewControllerRegistry registry) {
//		registry.addViewController("/").setViewName("inicio");
//		registry.addViewController("/inicio").setViewName("inicio");
//		registry.addViewController("/error").setViewName("error");
//		registry.addViewController("/persona/listado").setViewName("persona/listado");
//		registry.addViewController("/persona/new").setViewName("persona/new");
//		registry.addViewController("/persona/fichaDatos").setViewName("persona/fichaDatos");
//		registry.addViewController("/logout-success").setViewName("logout");
//	}

	@Override
	public void addInterceptors(final InterceptorRegistry registry) {
		registry.addInterceptor(new LocaleChangeInterceptor()).excludePathPatterns("/estatico/**");
	}

	@Bean
	public TilesViewResolver tilesViewResolver() {
		final TilesViewResolver tilesViewResolver = new TilesViewResolver();
		tilesViewResolver.setViewClass(TilesView.class);
		tilesViewResolver.setRequestContextAttribute("springRequestContext");
		return tilesViewResolver;
	}

	@Bean
	public TilesConfigurer tilesConfigurer() {
		final TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions(new String[] { "/WEB-INF/tiles-defs.xml" });
		return tilesConfigurer;
	}

	//	<!-- Jasper Reports -->
	//	<bean class="org.springframework.web.servlet.view.XmlViewResolver">
	//		<property name="location" value="/WEB-INF/jasper-views.xml" />
	//		<property name="order" value="0" />
	//	</bean>

	@Bean
	public XmlViewResolver xmlViewResolver() {
		final XmlViewResolver xmlViewResolver = new XmlViewResolver();
		Resource resource = new ClassPathResource("net/izfe/g240/wfiguiarapidaspringmvcweb/jasper-views.xml");
		xmlViewResolver.setLocation(resource);
		xmlViewResolver.setOrder(0);

		return xmlViewResolver;
	}

	@Bean
	public LocaleResolver localeResolver() {
		return new SessionLocaleResolver();
	}

	@Bean
	public HandlerExceptionResolver exceptionResolver() {
		final IzfeSimpleMappingExceptionResolver exceptionResolver = new IzfeSimpleMappingExceptionResolver();
		exceptionResolver.setLogCategory("net.izfe.g240.wfiguiarapidaspringmvcweb");

		final Properties exceptionMappings = new Properties();
		exceptionMappings.put("java.lang.Exception", "error");
		exceptionResolver.setExceptionMappings(exceptionMappings);

		return exceptionResolver;
	}

	@Override
	public void addArgumentResolvers(final List<HandlerMethodArgumentResolver> argumentResolvers) {
		final PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
		resolver.setFallbackPageable(new PageRequest(0, 10));
		argumentResolvers.add(resolver);
	}

	@Bean(name = "messageSource")
	public MessageSource messageSource() {
		final ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasenames("net/izfe/g240/wfiguiarapidaspringmvcweb/resources/ApplicationResources");
		source.setUseCodeAsDefaultMessage(true);
		source.setDefaultEncoding("UTF-8");
		return source;
	}

	/**
	 * Recupera el messageAccessor.
	 *
	 * @return messageAccessor
	 */
	@Bean(name = "messageAccessor")
	public MessageSourceAccessor messageSourceAccessor() {
		return new MessageSourceAccessor(this.messageSource());
	}

	@Bean
	public JndiObjectFactoryBean dataSource() {
		final JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
		jndiObjectFactoryBean.setJndiName("jdbc/WFIGuiaRapidaDS");
		jndiObjectFactoryBean.setResourceRef(true);
		return jndiObjectFactoryBean;
	}

	@Bean
	public NamedParameterJdbcTemplate namedJdbcTemplate(final DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}

	@Bean
	public JdbcTemplate jdbcTemplate(final DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean
	public DataSourceTransactionManager transactionManager(final DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

}
