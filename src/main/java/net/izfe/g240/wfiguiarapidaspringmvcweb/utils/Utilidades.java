package net.izfe.g240.wfiguiarapidaspringmvcweb.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * M�todos de uso general en la aplicacion
 * 
 * @author PINSAN1
 *
 */
public final class Utilidades {
	/**
	 * Constructor privado
	 */
	private Utilidades() {}
	
	/** Formato de fechas para mostrar	*/
	public static final String DATE_STRING_FORMAT = "yyyy-MM-dd";
	
	public static final String FECHA_INFINITA = "9999-12-31";

	/**
	 * Transforma la fecha en texto, siguiendo el formato definido
	 * 
	 * @param fecha
	 *            Fecha a formatear
	 * @param formato
	 *            Formato que debe seguir el texto resultante
	 * @return Texto que representa la fecha con el formato especificado
	 */
	public static String formatearFecha(Date fecha, String formato) {
		DateFormat df = new SimpleDateFormat(formato, new Locale("es", "ES"));
		String formateada;
		try {
			formateada = df.format(fecha);
		} catch (Exception ex) {
			formateada = FECHA_INFINITA;
		}
		return formateada;
	}

	private static final Date FECHA_INFINITA_DATE = Utilidades.parsearFecha(FECHA_INFINITA, "yyyy-MM-dd");
	/**
	 * Transforma un texto en la fecha, interpret�ndola con el formato
	 * suministrado
	 * 
	 * @param fecha
	 *            Texto de la fecha
	 * @param formato
	 *            Formato que se utiliza para interpretar el texto
	 * @return Un objeto de tipo Date con la fecha interpretada
	 */
	public static Date parsearFecha(String fecha, String formato) {
		DateFormat df = new SimpleDateFormat(formato, new Locale("es", "ES"));
		Date parseada;
		try {
			parseada = df.parse(fecha);
		} catch (ParseException pE) {
			parseada = FECHA_INFINITA_DATE;
		}
		return parseada;
	}
	
	public static String formatearFecha(String fecha) {
		return FECHA_INFINITA.equals(fecha) ? "" : fecha;
	}
}