package net.izfe.g240.wfiguiarapidaspringmvcweb;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.filter.CharacterEncodingFilter;

import net.izfe.g240.wfiframeworkizfelib.presentacion.listeners.Log4jLocalIzfeListener;
import net.izfe.g240.wfiframeworkizfelib.springmvc.AbstractIzfeDispatcherServletInitializer;


// TODO falta configuración de Spring Security y el filtro del Framework
public class GuiaRapidaInitializer extends AbstractIzfeDispatcherServletInitializer
    implements WebApplicationInitializer {

  @Override
  public void onStartup(final ServletContext servletContext) throws ServletException {
    super.onStartup(servletContext);
    
    servletContext.addListener(new Log4jLocalIzfeListener());
    //servletContext.addListener(new PersonaListener());
  }

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class[] { GuiaRapidaWeb.class };
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] { "/" };
  }

  @Override
  protected Filter[] getServletFilters() {
    final CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
    encodingFilter.setForceEncoding(true);
    encodingFilter.setEncoding("UTF-8");

    // DelegatingFilterProxy springSecurityFilter = new DelegatingFilterProxy();

    return new Filter[] { encodingFilter /* , springSecurityFilter */ };
  }

}
