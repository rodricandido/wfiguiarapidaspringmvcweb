package net.izfe.g240.wfiguiarapidaspringmvcweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**  */
@Controller
public class BaseController {

	/** Inyectar mediante @Autowired */
//	@Autowired
//	private BaseFacade baseFacade;

	@RequestMapping("/")
	public String home() {
		return "inicio";
	}	
	
	@RequestMapping("/inicio")
	public String inicio() {
		return "inicio";
	}

}
