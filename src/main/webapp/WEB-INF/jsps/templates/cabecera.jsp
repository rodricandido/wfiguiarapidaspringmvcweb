<%@ include file="taglibs.jsp" %>

<div id="header">
	<div id="logo">
		<h1>
			<spring:url var="url" value="/inicio"/>
			<a href="${url}">
				<spring:message code="Aplicacion"/> <span><spring:message code="Motivo"/></span>
			</a> 
		</h1>
		<p><spring:message code="AplicacionEjemploIZFE"/></p>
	</div>
</div>
<!-- end #header -->


