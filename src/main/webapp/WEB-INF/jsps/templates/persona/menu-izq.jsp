<%@ include file="../taglibs.jsp" %>

<div id="sidebar">
	<ul>
		<li>
			<h2><spring:message code="GestionPersonal" /></h2>
			<p><spring:message code="DescripcionPersonal" /></p>
			<p><spring:message code="GestionPersonal" /></p>
		</li>
		<li>
			<h2><spring:message code="Menu"/></h2>
			<ul>
				<li>
					<spring:url var="url" value=""/>
					<a href="${url}">
						<spring:message code="listadoPersonal"/>
					</a>
				</li>
				<li>
					<spring:url var="url" value="" />
					<a href="${url}">
						<spring:message code="registrarPersonal" />
					</a>
				</li>
				<li>
					<spring:url var="url" value="">
						<spring:param name="locale" value="${springRequestContext.locale == 'eu_ES'? 'es_ES' : 'eu_ES'}"/>
					</spring:url>
					<a href="${url}"> 
						<spring:message code="cambiarLocale" />
					</a>
				</li>
			</ul>
		</li>
	</ul>
</div>