<%@ include file="../templates/taglibs.jsp" %>

<p>El objetivo de este proyecto es servir como base para nuevos proyectos desarrollados sobre Spring MVC.</p> 

<p>Antes de comenzar a desarrollar el proyecto sobre esta base, hay que realizar los siguientes cambios:</p>
<ol>
	<li>Renombrar el proyecto al nombre que le corresponde</li>
	<li>Renombrar el context-root al nombre que le corresponde</li>
	<li>Abrir el fichero applicationContext.xml y actualizar los paquetes de context:component-scan</li>
	<li>Abrir el fichero spring-web-servlet y modificar la propiedad logCategory con el nombre de log (paquete) que le coresponde</li>
	<li>Abrir el fichero web.xml y actualizar el par�metro contextConfigLocation</li>
	<li>Desconectar el proyecto de SVN y subirlo al repositorio que le corresponde</li>
</ol>

<p>De esta manera el proyecto ya est� listo para desarrollar sobre �l.</p> 